Ansible Role: Docker Swarm
=========

Init swarm cluster

Requirements
------------

None

Role Variables
--------------
[defaults/main.yml](defaults/main.yml)

Dependencies
------------

[ansible_role_docker](https://gitlab.com/klovtsov/devops_includes/ansible_roles/ansible_role_docker)

Example Playbook
----------------

    - hosts: all
      become: true
      gather_facts: true
      roles:
        - ansible_role_docker_swarm

License
-------

MIT

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
